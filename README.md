# PhyloTraitSimulator
## Simulations of intra- and inter-specific trait evolution
This is the code used to simulate phenotypic data for the manuscript: “On the effect of asymmetric trait inheritance on models of trait evolution” by 
Pablo Duchen, Michael L. Alfaro, Jonathan Rolland, Nicolas Salamin, Daniele Silvestro.

Data available from the Dryad Digital Repository: https://doi.org/10.5061/dryad.s7h44j13v

### Requirements 
The simulator is written in R requires the libraries `ape`, `geiger`, and `optparse`.
The code was tested on different MacOS and Linux distributions using R 3.x.


### Usage

1) To display the full options type in a Terminal window:

`Rscript PhyloTraitSimulator.R --help`

2) To run a simulation for 100,000 generations (`-t 100000`), with random trait segregation at speciation (RSS, `-a 4`), flat fitness landscape (`-f 0`) and setting a fixed seed (`-r 152`):

`Rscript PhyloTraitSimulator.R -r 152 -t 100000 -a 1 -f 0 `

3) To run a similar simulation, with trait segregation at speciation (TSS, `-a 2`), normal fitness lanscape (`-f 1`) with a standard deviation set to 25 (`-c 25`),

`Rscript PhyloTraitSimulator.R -t 100000 -a 2 -f 1 -c 25`

4) To run a similar simulation, with mixed random and trait segregation at speciation (mixed, `-a 24`) where the probability of TSS is 5% (`-y 0.05`), and a flat fitness lanscape (`-f 0`),

`Rscript PhyloTraitSimulator.R -t 100000 -f 0 -a 24 -y 0.05`


(Note 1: The program will stop at the given number of generations or until 100 species have been generated. If you need to simulate more species then you can change this in line 541).

(Note 2: Some parameters are chosen randomly for every species from prior distributions. You can see these priors between lines 206-209.)

### Output files

One simulation run will generate the following output files:

1. `output_newickTreeTips_*` is a newick tree file with all extant lineages.

2. `output_traitsTips_*` is a text file including the final mean species phenotypes at the tips of the tree file in 1).

3. `output_newickTreeAll_*` is a newick tree file with all extant and extinct lineages.

4. `output_means_*` is a text file with mean species phenotypes at intermediate generations.

5. `output_vars_*` is text file with intraspecific phenotypic variance at intermediate generations.

6. `output_popSizes_*` is a text file with the population sizes of each species at intermediate generations.

7. `output_plots_*` is a PDF file with various summary plots, such as individual traits on the fitness curve at intermediate time points, trajectories of the mean and the variance phenotypes, phylogeny, distribution of species traits at the tips of the phylogeny.

8. `output_geography_*` is a text file with the geographic areas occupied by each individual at intermediate generations.

9. `output_species_*` is a text file with species specific parameters resulting from the simulation.

The `*` after each output file contains a suffix with the speciation type used (`a`), the fitness landscape (`f`), and the seed.
